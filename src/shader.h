#pragma once

class Shader {
  unsigned int program_id_;

public:
  Shader(const GLchar* vertexPath, const GLchar* fragmentPath);
  void use() const;
  void setBool(const std::string &name, bool value) const;
  void setInt(const std::string &name, int value) const;
  void setFloat(const std::string &name, float value) const;
  void setVec3(const std::string &name,
               float f1,
               float f2,
               float f3) const;
  void setVec3(const std::string &name, const glm::vec3 &v) const;
  void setVec4(const std::string &name,
               float f1,
               float f2,
               float f3,
               float f4) const;
  void setMat4(const std::string &name,
               const glm::mat4 &value) const;
};
