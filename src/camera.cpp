#include <glm/gtc/matrix_transform.hpp>
#include "camera.h"

Camera::Camera(const glm::vec3 &init_pos)
  : pos_(init_pos),
    front_(glm::vec3(0.0f, 0.0f, -1.0f)),
    up_(glm::vec3(0.0f, 1.0f, 0.0f)),
    fov_(45.0f),
    yaw_(-90.0f),
    pitch_(.0f)
{}

glm::mat4 Camera::GetViewMatrix() const {
  return glm::lookAt(pos_, pos_ + front_, up_);
}

float Camera::GetZoomLevel() const {
  return fov_;
}

glm::vec3 Camera::GetPosition() const {
  return pos_;
}

void Camera::MoveForward(float deltaTime) {
  pos_ += front_ * speed_factor * deltaTime;
}

void Camera::MoveBackward(float deltaTime) {
  pos_ -= front_ * speed_factor * deltaTime;
}

void Camera::MoveLeft(float deltaTime) {
  pos_ -= glm::normalize(glm::cross(front_, up_))
          * speed_factor * deltaTime;
}

void Camera::MoveRight(float deltaTime) {
  pos_ += glm::normalize(glm::cross(front_, up_))
          * speed_factor * deltaTime;
}

void Camera::OnMouseMove(float x_offset, float y_offset) {
  x_offset *= mouse_sensitivity;
  y_offset *= mouse_sensitivity;

  yaw_ += x_offset;
  pitch_ += y_offset;
  if(pitch_ > 89.0f) pitch_ = 89.0f;
  if(pitch_ < -89.0f) pitch_ = -89.0f;

  front_ = glm::normalize(
    glm::vec3(cos(glm::radians(pitch_)) * cos(glm::radians(yaw_)),
              sin(glm::radians(pitch_)),
              cos(glm::radians(pitch_)) * sin(glm::radians(yaw_))));
}

void Camera::OnMouseScroll(float offset) {
  if (fov_ >= fov_min && fov_ <= fov_max)
    fov_ -= offset;
  if (fov_ <= fov_min) fov_ = fov_min;
  if (fov_ >= fov_max) fov_ = fov_max;
}
