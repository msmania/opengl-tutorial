#include <glad/glad.h>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "shader.h"

static std::string read_from_file(const GLchar* path) {
  std::string data;
  std::ifstream ifile;
  ifile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
  try {
    std::stringstream ss;
    ifile.open(path);
    ss << ifile.rdbuf();
    ifile.close();
    data = ss.str();
  }
  catch(std::ifstream::failure e) {
    std::cout << "ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ" << std::endl;
  }
  return data;
}

static void show_info_log(GLuint shader, const char *message) {
  char log[512];
  glGetShaderInfoLog(shader, sizeof(log), nullptr, log);
  std::cout << message << std::endl << log << std::endl;
}

static unsigned int compile_shader(const char* shader_code, GLenum type) {
  unsigned int id = glCreateShader(type);
  glShaderSource(id, 1, &shader_code, nullptr);
  glCompileShader(id);

  int success;
  glGetShaderiv(id, GL_COMPILE_STATUS, &success);
  if(!success)
    show_info_log(id, "ERROR::SHADER::COMPILATION_FAILED");

  return id;
}

Shader::Shader(const GLchar* vertexPath, const GLchar* fragmentPath)
  : program_id_(glCreateProgram()) {
  const std::string
    vertexCode = read_from_file(vertexPath),
    fragmentCode = read_from_file(fragmentPath);
  const unsigned int
    vertex = compile_shader(vertexCode.c_str(), GL_VERTEX_SHADER),
    fragment = compile_shader(fragmentCode.c_str(), GL_FRAGMENT_SHADER);

  glAttachShader(program_id_, vertex);
  glAttachShader(program_id_, fragment);
  glLinkProgram(program_id_);

  int success;
  glGetProgramiv(program_id_, GL_LINK_STATUS, &success);
  if(!success)
    show_info_log(program_id_, "ERROR::SHADER::PROGRAM::LINKING_FAILED");

  glDeleteShader(vertex);
  glDeleteShader(fragment);
}

void Shader::use() const {
  glUseProgram(program_id_);
}

void Shader::setBool(const std::string &name, bool value) const {
  glUniform1i(glGetUniformLocation(program_id_, name.c_str()),
              value ? 1 : 0);
}
void Shader::setInt(const std::string &name, int value) const {
  glUniform1i(glGetUniformLocation(program_id_, name.c_str()),
              value);
}
void Shader::setFloat(const std::string &name, float value) const {
  glUniform1f(glGetUniformLocation(program_id_, name.c_str()),
              value);
}

void Shader::setVec3(const std::string &name,
                     float f1,
                     float f2,
                     float f3) const {
  glUniform3f(glGetUniformLocation(program_id_, name.c_str()),
              f1, f2, f3);
}

void Shader::setVec3(const std::string &name, const glm::vec3 &v) const {
  glUniform3f(glGetUniformLocation(program_id_, name.c_str()),
              v.x, v.y, v.z);
}

void Shader::setVec4(const std::string &name,
                     float f1,
                     float f2,
                     float f3,
                     float f4) const {
  glUniform4f(glGetUniformLocation(program_id_, name.c_str()),
              f1, f2, f3, f4);
}

void Shader::setMat4(const std::string &name,
                     const glm::mat4 &value) const {
  glUniformMatrix4fv(glGetUniformLocation(program_id_, name.c_str()),
                     1,
                     GL_FALSE,
                     glm::value_ptr(value));
}
