#pragma once

class Camera {
  static constexpr float speed_factor = 2.5f;
  static constexpr float mouse_sensitivity = .5f;
  static constexpr float fov_min = 0.1f;
  static constexpr float fov_max = 80.0f;
  glm::vec3 pos_;
  glm::vec3 front_;
  const glm::vec3 up_;
  float fov_;
  float yaw_;
  float pitch_;

public:
  Camera(const glm::vec3 &init_pos);
  glm::mat4 GetViewMatrix() const;
  float GetZoomLevel() const;
  glm::vec3 GetPosition() const;
  void MoveForward(float deltaTime);
  void MoveBackward(float deltaTime);
  void MoveLeft(float deltaTime);
  void MoveRight(float deltaTime);
  void OnMouseMove(float x_offset, float y_offset);
  void OnMouseScroll(float offset);
};
