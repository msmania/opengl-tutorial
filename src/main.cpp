#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <iomanip>
#include <glm/gtc/matrix_transform.hpp>
#include "shader.h"
#include "camera.h"

unsigned int loadTexture(const char *path);

constexpr int screenWidth = 800, screenHeight = 600;
float deltaTime = 0.0f, // Time between current frame and last frame
      lastFrame = 0.0f, // Time of last frame
      lastX = screenWidth / 2, lastY = screenHeight / 2;
bool firstMouse = true;
Camera camera(glm::vec3(0.0f, .0f, 6.0f));

void framebuffer_size_callback(GLFWwindow* window, int width, int height) {
  glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window) {
  if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    glfwSetWindowShouldClose(window, true);
  if (glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS)
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  if (glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS)
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

  if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    camera.MoveForward(deltaTime);
  if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    camera.MoveBackward(deltaTime);
  if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    camera.MoveLeft(deltaTime);
  if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
    camera.MoveRight(deltaTime);
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos) {
  if (firstMouse) {
    lastX = static_cast<float>(xpos);
    lastY = static_cast<float>(ypos);
    firstMouse = false;
  }

  float xoffset = static_cast<float>(xpos) - lastX,
        yoffset = lastY - static_cast<float>(ypos);

  lastX = static_cast<float>(xpos);
  lastY = static_cast<float>(ypos);

  camera.OnMouseMove(xoffset, yoffset);
}

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset) {
  camera.OnMouseScroll(static_cast<float>(yoffset));
}

void print_matrix(const glm::mat4 &m) {
  constexpr int W = 8;
  for (int i = 0; i < 4; ++i) {
    for (int j = 0; j < 4; ++j)
      std::cout
        << std::setw(W + 4) << std::setprecision(W)
        << m[j][i];
    std::cout << ';' << std::endl;
  }
}

int main(int argc, char **argv) {
/*
  auto proj = glm::perspective(glm::radians(45.0f),
                               static_cast<float>(screenWidth) / screenHeight,
                               0.1f,
                               100.0f);
  print_matrix(proj);
*/

  glfwInit();
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  //glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

  GLFWwindow* window = glfwCreateWindow(screenWidth,
                                        screenHeight,
                                        "LearnOpenGL",
                                        NULL,
                                        NULL);
  if (window == NULL) {
      std::cout << "Failed to create GLFW window" << std::endl;
      glfwTerminate();
      return -1;
  }
  glfwMakeContextCurrent(window);
  glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
  glfwSetCursorPosCallback(window, mouse_callback);
  glfwSetScrollCallback(window, scroll_callback);

  if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
      std::cout << "Failed to initialize GLAD" << std::endl;
      return -1;
  }

  glEnable(GL_DEPTH_TEST);

  unsigned int texture1 = loadTexture("container2.jpg"),
               texture2 = loadTexture("container2_specular.png");

  float vertices[] = {
    -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 0.0f,
     0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 0.0f,
     0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 1.0f,
     0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 1.0f,
    -0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 1.0f,
    -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 0.0f,

    -0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   0.0f, 0.0f,
     0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   1.0f, 0.0f,
     0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   1.0f, 1.0f,
     0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   1.0f, 1.0f,
    -0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   0.0f, 1.0f,
    -0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   0.0f, 0.0f,

    -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f, 0.0f,
    -0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  1.0f, 1.0f,
    -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
    -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
    -0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  0.0f, 0.0f,
    -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f, 0.0f,

     0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f,
     0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f,
     0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
     0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
     0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  0.0f, 0.0f,
     0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f,

    -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f, 1.0f,
     0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  1.0f, 1.0f,
     0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f, 0.0f,
     0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f, 0.0f,
    -0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  0.0f, 0.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f, 1.0f,

    -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f, 1.0f,
     0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  1.0f, 1.0f,
     0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f, 0.0f,
     0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f, 0.0f,
    -0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  0.0f, 0.0f,
    -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f, 1.0f
  };

  const glm::vec3 cubePositions[] = {
    glm::vec3( 0.0f,  0.0f,  0.0f),
    glm::vec3( 2.0f,  5.0f, -15.0f),
    glm::vec3(-1.5f, -2.2f, -2.5f),
    glm::vec3(-3.8f, -2.0f, -12.3f),
    glm::vec3( 2.4f, -0.4f, -3.5f),
    glm::vec3(-1.7f,  3.0f, -7.5f),
    glm::vec3( 1.3f, -2.0f, -2.5f),
    glm::vec3( 1.5f,  2.0f, -2.5f),
    glm::vec3( 1.5f,  0.2f, -1.5f),
    glm::vec3(-1.3f,  1.0f, -1.5f),
  };

  unsigned int VAO[2];
  glGenVertexArrays(2, VAO);
  std::cout << "VAO = " << VAO[0] << std::endl;

  unsigned int VBO[1];
  glGenBuffers(1, VBO);
  std::cout << "VBO = " << VBO[0] << std::endl;

  glBindVertexArray(VAO[0]);
  glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(sizeof(float) * 3));
  glEnableVertexAttribArray(1);
  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(sizeof(float) * 6));
  glEnableVertexAttribArray(2);

  glBindVertexArray(VAO[1]);
  glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);

  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);

  int nrAttributes;
  glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &nrAttributes);
  std::cout << "Maximum nr of vertex attributes supported: " << nrAttributes << std::endl;

  const glm::vec3 white{1.0f, 1.0f, 1.0f},
                  blue{.0f, .0f, 1.0f},
                  red{1.0f, .0f, .0f},
                  spotLight{5.0f, 2.0f, 1.0f};

  Shader shader1("vertex.shader", "fragment1.shader"),
         shader2("vertex.shader", "fragment2.shader");
  shader1.use();
  shader1.setInt("material.diffuse", 0);
  shader1.setInt("material.specular", 1);
  shader1.setFloat("material.shininess", 32.0f);

  shader1.setVec3("dirLight.direction", -1.0f, .0f, .0f);
  shader1.setVec3("dirLight.ambient", white * .1f);
  shader1.setVec3("dirLight.diffuse", white * .4f);
  shader1.setVec3("dirLight.specular", white * .5f);

  shader1.setFloat("pointLights[0].constant", 1.0f);
  shader1.setFloat("pointLights[0].linear", 0.09f);
  shader1.setFloat("pointLights[0].quadratic", 0.032f);
  shader1.setVec3("pointLights[0].ambient", blue * .1f);
  shader1.setVec3("pointLights[0].diffuse", blue * .5f);
  shader1.setVec3("pointLights[0].specular", 1.0f, 1.0f, 1.0f);

  shader1.setVec3("spotLight.position", spotLight);
  shader1.setVec3("spotLight.direction", -1.0f, -.1f, -1.0f);
  shader1.setFloat("spotLight.cutOff", glm::cos(glm::radians(5.0f)));
  shader1.setFloat("spotLight.outerCutOff", glm::cos(glm::radians(7.0f)));
  shader1.setFloat("spotLight.constant", 1.0f);
  shader1.setFloat("spotLight.linear", 0.05f);
  shader1.setFloat("spotLight.quadratic", 0.032f);
  shader1.setVec3("spotLight.ambient", white * .0f);
  shader1.setVec3("spotLight.diffuse", red);
  shader1.setVec3("spotLight.specular", 1.0f, 1.0f, 1.0f);

  while(!glfwWindowShouldClose(window)) {
    const auto currentFrame = static_cast<float>(glfwGetTime());
    deltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;
    processInput(window);

    glClearColor(.0f, .0f, .0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    const auto view = camera.GetViewMatrix(),
               projection = glm::perspective(glm::radians(camera.GetZoomLevel()),
                                             static_cast<float>(screenWidth) / screenHeight,
                                             0.1f,
                                             100.0f);
    const auto time_f = static_cast<float>(glfwGetTime());
    const auto lightPos = glm::vec3(5.0f * sin(time_f),
                                    .0f,
                                    5.0f * cos(time_f));
    glm::mat4 model;

    shader1.use();
    shader1.setVec3("viewPos", camera.GetPosition());
    shader1.setMat4("view", view);
    shader1.setMat4("projection", projection);
    shader1.setVec3("pointLights[0].position", lightPos);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture1);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, texture2);
    glBindVertexArray(VAO[0]);

    int cnt = 0;
    for (const auto &position : cubePositions) {
      model = glm::translate(glm::mat4(), position);
      model = glm::rotate(model,
                          glm::radians(20.0f * cnt),
                          glm::vec3(1.0f, 0.3f, 0.5f));
      model = glm::rotate(model, time_f * .4f, glm::vec3(1.0f, .0f, .0f));
      model = glm::rotate(model, time_f * .5f, glm::vec3(.0f, .0f, 1.0f));
      shader1.setMat4("model", model);
      glDrawArrays(GL_TRIANGLES, 0, 36);
      ++cnt;
    }

    model = glm::translate(glm::mat4(), lightPos);
    model = glm::scale(model, glm::vec3(0.2f));

    shader2.use();
    shader2.setMat4("model", model);
    shader2.setMat4("view", view);
    shader2.setMat4("projection", projection);
    shader2.setVec3("solidColor", blue);
    glBindVertexArray(VAO[1]);
    glDrawArrays(GL_TRIANGLES, 0, 36);

    model = glm::translate(glm::mat4(), spotLight);
    model = glm::scale(model, glm::vec3(0.2f));
    shader2.setMat4("model", model);
    shader2.setVec3("solidColor", red);
    glDrawArrays(GL_TRIANGLES, 0, 36);

    glfwSwapBuffers(window);
    glfwPollEvents();
  }

  glDeleteVertexArrays(2, VAO);
  glDeleteBuffers(1, VBO);
  glDeleteTextures(1, &texture1);
  glDeleteTextures(1, &texture2);

  glfwTerminate();
  return 0;
}
